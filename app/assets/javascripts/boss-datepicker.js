function build_date_picker($container) {
  var $input      = $container.find('input[type="text"]');
  var $valueField = $container.find('.js-date-value');

  if (typeof $input.datepicker === 'function') {

    if ($valueField.length === 0) {
      $valueField = $('<input/>', {
        type:  'hidden',
        name:  $input.attr('name'),
        value: $input.attr('value'),
        class: 'js-date-value ' + $input.attr('class')
      });

      $container.append($valueField);
    }

    $input.removeAttr('name');
    $input.removeClass('value');

    if ($input.val() !== undefined && /\-/.test($input.val())) {
      $input.val(moment($input.val(), 'YYYY-MM-DD').format('MM/DD/YYYY'));
      $valueField.val(moment($input.val(), 'MM/DD/YYYY').format('YYYY-MM-DD'));
    }

    $container.datepicker({
      forceParse: false,
      autoclose: true
    }).on('changeDate', function(e) {
      $valueField.val(moment(e.date).format('YYYY-MM-DD'));
    }).on('clearDate', function(e) {
      $valueField.val('');
    });
  }
}

$(document).ready(function () {
  $('.input-group.date').each(function() {
    build_date_picker($(this));
  });
});
