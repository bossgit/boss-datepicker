# Boss::Datepicker

This gem simplifies datepicker formatting handling. It enables the user to see their date in the US format (MM/DD/YYYY) but in the background, in an hidden field, a date with the standard YYYY-MM-DD format will be saved and sent to the server as a parameter. This way you can be sure that your server will always get the standard format and it will simplify logic in the server.

## Installation

Add these lines to your application's Gemfile (boss-datepicker depends on jQuery and Moment.js):

```ruby
gem 'jquery-rails'
gem 'momentjs-rails'
gem 'boss-datepicker', git: 'https://bitbucket.org/bossgit/boss-datepicker.git', tag: 'v1.0.0'
```

And then execute:

    $ bundle


## Usage

Require jQuery, Moment.js and it in your application's application.js file:
```javascript
//= require jquery
//= require moment
//= require boss-datepicker
```

The gem requires you to use input groups for date inputs like this:
```html
<div class="input-group date">
  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
  <input class="form-control" type="text">
</div>
```

Datepicker will be automatically enabled on all containers with the class "input-group date". If you want to manually enable it then do it like this:
```javascript
build_date_picker($('.input-group-selector'));
```

## Versioning

This gem uses [semantic versioning](http://semver.org) which means, given a version number MAJOR.MINOR.PATCH, increment the:

1. MAJOR version when you make incompatible API changes,
2. MINOR version when you add functionality in a backwards-compatible manner, and
3. PATCH version when you make backwards-compatible bug fixes.
