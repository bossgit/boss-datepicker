# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'boss/datepicker/version'

Gem::Specification.new do |spec|
  spec.name          = "boss-datepicker"
  spec.version       = Boss::Datepicker::VERSION
  spec.authors       = ["Jaanus Jaakma"]
  spec.email         = ["jaanus@glenworx.com"]

  spec.summary       = %q{Datepicker component based on bootstrap-datepicker with custom behaviour.}
  spec.description   = <<-EOF
    This datepicker includes a hidden field that will always keep the date in YYYY-MM-DD format even if it is displayed in MM/DD/YYYY.
    That way you don't need server side formatting for date fields, since the date sent to server is always in the standard YYYY-MM-DD.
  EOF
  spec.homepage      = "https://bitbucket.org/bossgit/boss-datepicker"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://gems.boss-solutions.com"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'railties', '>= 4.0', '< 5.1'
  spec.add_dependency 'jquery-rails', '~> 4.0'
  spec.add_dependency 'momentjs-rails', '~> 2.10'
  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
end
